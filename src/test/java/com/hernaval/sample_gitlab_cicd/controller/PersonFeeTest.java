package com.hernaval.sample_gitlab_cicd.controller;

import com.hernaval.sample_gitlab_cicd.FeeService;
import com.hernaval.sample_gitlab_cicd.model.Employe;
import com.hernaval.sample_gitlab_cicd.model.Founder;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.ComponentScan;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertTrue;

@WebMvcTest
@ComponentScan(basePackages = "com.hernaval.sample_gitlab_cicd")
public class PersonFeeTest {
    private  static final Logger LOGGER = LoggerFactory.getLogger(PersonFeeTest.class);

    @Autowired
    FeeService feeService;


    @Test
    void shouldEmployeHasPositiveBenefit(){
        Employe emp = Employe.builder().jobName("dev").salary(500.5f).depense(300).build();
        float benefit = feeService.calculate(emp.getSalary(), emp.getDepense());

        assertTrue(benefit > 0);
    }

    @Test
    void shouldFounderHasPositiveBenefit(){
        Employe emp = Employe.builder().jobName("dev").salary(500.5f).build();
        Employe emp2 = Employe.builder().jobName("dev").salary(700.5f).build();
        Founder f = Founder.builder().profit(1500.5f).employes(Arrays.asList(emp,emp2)).build();

        LOGGER.info(String.valueOf(f.getProfit()));

        assertTrue(feeService.calculateForFounder(f.getEmployes(), f.getProfit()) > 0);
    }
}
