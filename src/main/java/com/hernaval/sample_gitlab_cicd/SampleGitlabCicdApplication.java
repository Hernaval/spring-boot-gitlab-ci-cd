package com.hernaval.sample_gitlab_cicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleGitlabCicdApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleGitlabCicdApplication.class, args);
	}

}
