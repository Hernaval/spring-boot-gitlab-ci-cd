package com.hernaval.sample_gitlab_cicd;

import com.hernaval.sample_gitlab_cicd.model.Employe;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FeeService {
    public float calculate(float salary, float depense){
        return salary - depense;
    }

    public float calculateForFounder(List<Employe> employeList, float profit){
        float employeSalarie = employeList
                .stream()
                .map(employe -> employe.getSalary())
                .reduce(0.0F, Float::sum);

        return profit - employeSalarie;
    }


}
