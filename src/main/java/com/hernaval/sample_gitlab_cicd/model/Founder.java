package com.hernaval.sample_gitlab_cicd.model;

import com.hernaval.sample_gitlab_cicd.FeeService;
import jdk.jfr.DataAmount;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@AllArgsConstructor
@Data
@Getter
@Setter
@Builder
public class Founder extends Person implements FeeCalculation{
    private String company;
    private float profit;
    List<Employe> employes;

    @Autowired
    FeeService feeService;

    @Override
    public float calculateBenefit() {
        return feeService.calculateForFounder(this.employes, this.profit);
    }
}
