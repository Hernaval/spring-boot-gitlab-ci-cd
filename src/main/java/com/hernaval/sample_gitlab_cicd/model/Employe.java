package com.hernaval.sample_gitlab_cicd.model;

import com.hernaval.sample_gitlab_cicd.FeeService;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;

@AllArgsConstructor
@Getter
@Setter
@Data
@Builder

public class Employe extends Person implements FeeCalculation{
    private String jobName;
    private int experienceYear;
    private float salary;
    private float depense;

    public Employe(){
        super();
    }

    @Autowired
    FeeService feeService;

    @Override
    public float calculateBenefit() {
        return feeService.calculate(this.salary, this.depense) ;
    }
}
