package com.hernaval.sample_gitlab_cicd.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
abstract public class Person {
    protected String name;
    protected int age;
}
