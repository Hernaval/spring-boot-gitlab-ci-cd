package com.hernaval.sample_gitlab_cicd.model;

public interface FeeCalculation {
    float calculateBenefit();
}
